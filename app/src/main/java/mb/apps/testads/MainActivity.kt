package mb.apps.testads

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.unity3d.ads.UnityAds
import com.unity3d.ads.mediation.IUnityAdsExtendedListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity:AppCompatActivity(), IUnityAdsExtendedListener
{
    private val UNITY_ADS_ID = ""

    override fun onCreate(savedInstanceState:Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        UnityAds.initialize(this, UNITY_ADS_ID, this, false)

        pictureZoneButton.setOnClickListener {
            showAd(R.string.unityAdPicture)
        }
        rewardedVideoZoneButton.setOnClickListener {
            showAd(R.string.unityAdRewardVideo)
        }
        defaultVideoAndPictureButton.setOnClickListener {
            showAd(R.string.unityAdVideoAndPicture)
        }
        videoButton.setOnClickListener {
            showAd(R.string.unityAdVideo)
        }
        rewardedVideoButton.setOnClickListener {
            showAd(R.string.unityRewardedVideo)
        }
        interstitialVideoButton.setOnClickListener {
            showAd(R.string.unityAdFullScreen)
        }
    }

    private fun showAd(@StringRes placementId:Int)
    {
        val zone = getString(placementId)
        if(UnityAds.isReady(zone))
        {
            UnityAds.show(this, zone)
        }
        else
        {
            UnityAds.load(zone)
            Toast.makeText(this, "$zone -> No ads or still loading", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onUnityAdsPlacementStateChanged(placementId:String?, oldState:UnityAds.PlacementState?, newState:UnityAds.PlacementState?)
    {
        newState?.let {
            if(it == UnityAds.PlacementState.READY && oldState != newState)
            {
                Toast.makeText(this, "$placementId -> Loaded", Toast.LENGTH_SHORT).show()
            }
            else if(it == UnityAds.PlacementState.NO_FILL && oldState != newState)
            {
                Toast.makeText(this, "$placementId -> No Fill", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onUnityAdsClick(placementId:String?)
    {

    }

    override fun onUnityAdsStart(placementId:String?)
    {

    }

    override fun onUnityAdsFinish(placementId:String?, result:UnityAds.FinishState?)
    {

    }

    override fun onUnityAdsError(error:UnityAds.UnityAdsError?, message:String?)
    {
        message?.let {
            Toast.makeText(this, "Error loading Ad: $message", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onUnityAdsReady(placementId:String?)
    {
        placementId?.let {
            Toast.makeText(this, "$placementId Ads ready", Toast.LENGTH_SHORT).show()
        }
    }
}